package org.joget;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.joget.apps.app.service.AppPluginUtil;
import org.joget.apps.app.service.AppUtil;
import org.json.simple.JSONObject;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Properties;

public class MobileAppAuthentication extends  org.joget.plugin.base.ExtDefaultPlugin implements  org.joget.plugin.base.PluginWebSupport {


    private final static String MESSAGE_PATH = "message/MobileAppAuthentication";

    public String getName() {
        return "MobileAppAuthentication";
    }


    public String getVersion() {
        return "6.0.0";
    }


    public String getClassName() {
        return getClass().getName();
    }


    public String getLabel() {
        //support i18n
        return AppPluginUtil.getMessage("org.joget.MobileAppAuthentication.pluginLabel", getClassName(), MESSAGE_PATH);
    }

    public String getDescription() {
        //support i18n
        return AppPluginUtil.getMessage("org.joget.MobileAppAuthentication.pluginDesc", getClassName(), MESSAGE_PATH);
    }

    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String message;
        JSONObject fullJSON = new JSONObject();

        /*
            JSONObject errorJSON = new JSONObject();
            errorJSON.put("date", "");
            errorJSON.put("code", "2001");
            errorJSON.put("message", "Username is not a valid email address.");

            fullJSON.put("error",errorJSON);

            message = fullJSON.toString();
            out.write(message);
            out.flush();


         */
        PrintWriter out = response.getWriter();

         /* Get username and password from request */

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        /* Validate the username sent from apps */

        if(!username.contains("@")){

            fullJSON.put("Status", "Authentication Failed");
            fullJSON.put("code", "2001");
            fullJSON.put("message", "Username is not a valid email address.");

            message = fullJSON.toString();
            out.write(message);
            out.flush();

            return;
        }

        String realUsername = "";
        int i=0;
        while (username.charAt(i) != '@'){
            realUsername = realUsername + username.charAt(i);
            i++;
        }

        /* Validate the generated username sent from apps */

        if (realUsername.equals("")){

            fullJSON.put("Status", "Authentication Failed");
            fullJSON.put("code", "2001");
            fullJSON.put("message", "Username is not a valid email address.");

            message = fullJSON.toString();
            out.write(message);
            out.flush();

            return;
        }

        Properties initialProperties = new Properties();
        initialProperties.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
        initialProperties.put(Context.PROVIDER_URL, "ldap://localhost:389");
        initialProperties.put(Context.SECURITY_AUTHENTICATION, "simple");
        initialProperties.put(Context.SECURITY_PRINCIPAL, "CN=M.Agha,CN=Users,dc=fypworkflow,dc=westeurope,dc=cloudapp,dc=azure,dc=com");
        initialProperties.put(Context.SECURITY_CREDENTIALS, "Fyp-workflow1718");
        DirContext context = null;

        /* Connection to active directory with admin binding */

        try {
            context = new InitialDirContext(initialProperties);
        } catch (NamingException e) {

            fullJSON.put("Status", "Authentication Failed");
            fullJSON.put("code", "2002");
            fullJSON.put("message", "Naming Exception in admin connection to active directory.");

            message = fullJSON.toString();
            out.write(message);
            out.flush();

            return;
        }

        String searchFilter="(CN="+realUsername+")";
        String[] requiredAttributes={"mail"};
        SearchControls controls=new SearchControls();
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        controls.setReturningAttributes(requiredAttributes);
        NamingEnumeration users=null;

        /* Search for the user in active directory to make sure username is in active directory */

        try {
            users = context.search("CN=Users,dc=fypworkflow,dc=westeurope,dc=cloudapp,dc=azure,dc=com", searchFilter, controls);
        } catch (NamingException e) {

            fullJSON.put("Status", "Authentication Failed");
            fullJSON.put("code", "2003");
            fullJSON.put("message", "Naming Exception in username search in active directory.");

            message = fullJSON.toString();
            out.write(message);
            out.flush();

            return;
        }

         /* Make sure user object is not null */

        SearchResult searchResult=null;
        String email=null;
        if(users==null) {

            fullJSON.put("Status", "Authentication Failed");
            fullJSON.put("code", "2004");
            fullJSON.put("message", "User object is null.");

            message = fullJSON.toString();
            out.write(message);
            out.flush();

            return;
        }

         /* Check if username was found in active directory */

        try {
           //output.append("before in results\n");
           if(!users.hasMore()){

               fullJSON.put("Status", "Authentication Failed");
               fullJSON.put("code", "2005");
               fullJSON.put("message", "Incorrect Username/Password Combination.");

               message = fullJSON.toString();
               out.write(message);
               out.flush();

               return;
            }
        } catch (NamingException e) {

            fullJSON.put("Status", "Authentication Failed");
            fullJSON.put("code", "2006");
            fullJSON.put("message", "Naming Exception in username check returned from active directory.");

            message = fullJSON.toString();
            out.write(message);
            out.flush();

            return;
        }

         /* Authenticate */

        DirContext newContext = null;
        try {
            Properties newInitialProperties = new Properties();
            newInitialProperties.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
            newInitialProperties.put(Context.PROVIDER_URL, "ldap://localhost:389");
            newInitialProperties.put(Context.SECURITY_AUTHENTICATION, "simple");
            newInitialProperties.put(Context.SECURITY_PRINCIPAL, "CN=" + realUsername + ",CN=Users,dc=fypworkflow,dc=westeurope,dc=cloudapp,dc=azure,dc=com");
            newInitialProperties.put(Context.SECURITY_CREDENTIALS, password);

            newContext = new InitialDirContext(newInitialProperties);

        } catch (NamingException e) {

            fullJSON.put("Status", "Authentication Failed");
            fullJSON.put("code", "2005");
            fullJSON.put("message", "Incorrect Username/Password Combination.");

            message = fullJSON.toString();
            out.write(message);
            out.flush();

            return;
        }

         /* Success */

        fullJSON.put("Status", "Authentication Success");
        fullJSON.put("code", "2000");
        fullJSON.put("message", "");

        message = fullJSON.toString();
        out.write(message);
        out.flush();

        return;
    }


}
