package org.joget;

import org.joget.apps.app.service.AppPluginUtil;
import org.joget.apps.app.service.AppUtil;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;


//To login a user programatically
import org.joget.apps.workflow.security.WorkflowUserDetails;
import org.joget.commons.util.StringUtil;
import org.joget.directory.model.service.DirectoryManager;
import org.joget.directory.model.service.DirectoryUtil;
import org.joget.directory.model.service.UserSecurity;
import org.joget.workflow.model.service.WorkflowUserManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
import org.joget.directory.model.User;
import org.joget.workflow.util.WorkflowUtil;
import org.springframework.security.core.context.SecurityContextHolder;
import javax.servlet.http.HttpSession;
//import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import org.joget.directory.dao.UserDao;
import org.joget.directory.dao.RoleDao;
import java.util.UUID;

public class CasWebServicePlugin extends  org.joget.plugin.base.ExtDefaultPlugin implements  org.joget.plugin.base.PluginWebSupport {

    private final static String MESSAGE_PATH = "message/CasWebServicePlugin";

	String firstName;
	String lastName;
	String email;
    public String getName() {
        return "CasWebServicePlugin";
    }


    public String getVersion() {
        return "6.0.0";
    }


    public String getClassName() {
        return getClass().getName();
    }


    public String getLabel() {
        //support i18n
        return AppPluginUtil.getMessage("org.joget.CasWebServicePlugin.pluginLabel", getClassName(), MESSAGE_PATH);
    }

    public String getDescription() {
        //support i18n
        return AppPluginUtil.getMessage("org.joget.CasWebServicePlugin.pluginDesc", getClassName(), MESSAGE_PATH);
    }

    public String getPropertyOptions() {
        return AppUtil.readPluginResource(getClassName(), "/properties/CasWebServicePlugin.json", null, true, MESSAGE_PATH);
    }


    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    	if ("login".equals(request.getParameter("action"))){
			File file = new File("outFromCasPluginLogin.txt");
			PrintStream output = new PrintStream(file);
			output.append("This is from CasPlugin with action test\n");
			//login a user programatically
			//Get service beans



			DirectoryManager dm = (DirectoryManager) AppUtil.getApplicationContext().getBean("directoryManager");
			WorkflowUserManager workflowUserManager = (WorkflowUserManager) AppUtil.getApplicationContext().getBean("workflowUserManager");
			 
			//Login as "clark"
			String username = request.getRemoteUser();
			if (username != null && !username.isEmpty()){
				User user = dm.getUserByUsername(username);
				String currentLoggedInUser=workflowUserManager.getCurrentThreadUser();
				output.append("Before to check current user: "+currentLoggedInUser+"\n");
				output.append("Want to login User: "+username+"\n");
				if(!currentLoggedInUser.equals(username)) {
					output.append("New User\n");
					UserSecurity us = DirectoryUtil.getUserSecurity();


					if (user == null) {
						output.append("UserNot in database!\n");
						UserDao usersDAO = (UserDao) AppUtil.getApplicationContext().getBean("userDao");
						RoleDao rolesDAO = (RoleDao) AppUtil.getApplicationContext().getBean("roleDao");
						User newUser = new User();

						int count = 0;
						boolean userDataRetrieved = false;
						while (count < 3) {
							if (getUserData(username))
							{
								userDataRetrieved = true;
								break;
							}
							else count++;
						}
						if (!userDataRetrieved) {
							response.sendRedirect("https://fypworkflow.westeurope.cloudapp.azure.com:8443/jw/web/index");
							return;
						}
						newUser.setId(username);
						newUser.setUsername(username);
						newUser.setFirstName(firstName);
						newUser.setLastName(lastName);
						newUser.setEmail(email);
						String pass = UUID.randomUUID().toString();

						newUser.setConfirmPassword(pass);

						if (us != null) {
							newUser.setPassword(us.encryptPassword(newUser.getUsername(), pass));
						} else {
							//md5 password
							newUser.setPassword(StringUtil.md5Base16(pass));
						}

						Set roles = new HashSet();
						roles.add(rolesDAO.getRole("ROLE_USER"));
						newUser.setRoles(roles);
						newUser.setActive(1);
						usersDAO.addUser(newUser);
						user = newUser;
					}


					WorkflowUserDetails userDetail = new WorkflowUserDetails(user);
					output.append("Before to check current user: " + workflowUserManager.getCurrentThreadUser() + "\n");
					//Generate an authentication token without a password
					UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(userDetail.getUsername(), "", userDetail.getAuthorities());
					auth.setDetails(userDetail);
					//Login the user
					SecurityContextHolder.getContext().setAuthentication(auth);
					workflowUserManager.setCurrentThreadUser(user.getUsername());
					output.append("After to check current user: " + workflowUserManager.getCurrentThreadUser() + "\n");
					// generate new session to avoid session fixation vulnerability
				/*
				HttpServletRequest httpRequest = WorkflowUtil.getHttpServletRequest();
				HttpSession session = httpRequest.getSession(false);
				if (session != null) {
					SavedRequest savedRequest = (SavedRequest) session.getAttribute("SPRING_SECURITY_SAVED_REQUEST_KEY");
					session.invalidate();
					session = httpRequest.getSession(true);
					if (savedRequest != null) {
						session.setAttribute("SPRING_SECURITY_SAVED_REQUEST_KEY", savedRequest);
					}
				}

				*/
				}
				response.sendRedirect("https://fypworkflow.westeurope.cloudapp.azure.com:8443/jw/web/index");
			}
			else {
				output.append("Error, Null username used in log in process.\n");
			}
			
		}
		else if ("logout".equals(request.getParameter("action"))){
			File file = new File("outFromCasPluginTestOfCasLogout.txt");
			PrintStream output = new PrintStream(file);
			output.append("Success\n");
			response.sendRedirect("https://fypworkflow.westeurope.cloudapp.azure.com:8443/cas/logout");
			
    }
}
	public boolean getUserData(String username){
		
			PrintStream output = System.out;
			Properties initialProperties = new Properties();
			initialProperties.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
			initialProperties.put(Context.PROVIDER_URL, "ldap://localhost:389");
			initialProperties.put(Context.SECURITY_AUTHENTICATION, "simple");
			initialProperties.put(Context.SECURITY_PRINCIPAL, "CN=M.Agha,CN=Users,dc=fypworkflow,dc=westeurope,dc=cloudapp,dc=azure,dc=com");
			initialProperties.put(Context.SECURITY_CREDENTIALS, "Fyp-workflow1718");
			DirContext context = null;
			output.append("before context\n");
			try {
				 context = new InitialDirContext(initialProperties);
			} catch (NamingException e) {
				e.printStackTrace();
				return false;
			}
			output.append("after context\n");
			String searchFilter="(CN="+username+")";
			String[] requiredAttributes={"mail","givenName","sn"};
			SearchControls controls=new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			controls.setReturningAttributes(requiredAttributes);
			NamingEnumeration users=null;
			output.append("before search\n");
			if(context==null){
				output.append("context==null\n");
				return false;
			}
			try {
				users = context.search("CN=Users,dc=fypworkflow,dc=westeurope,dc=cloudapp,dc=azure,dc=com", searchFilter, controls);
			} catch (NamingException e) {
				e.printStackTrace();
				return false;
			}
			
			output.append("after search\n");
			SearchResult searchResult=null;
			email=null;
			firstName=null;
			lastName=null;
			if(users==null){
				output.append("users==null\n");
				return false;
			}
			try {
				output.append("before in results\n");
				if(users.hasMore())
                {
					output.append("in results\n");
                    searchResult=(SearchResult) users.next();
                    Attributes attr=searchResult.getAttributes();

                    try {
                        email=attr.get("mail").get(0).toString();
                    } catch (NamingException e) {
                        e.printStackTrace();
						return false;
                    }
                    try {
                        firstName=attr.get("givenName").get(0).toString();
                    } catch (NamingException e) {
                        e.printStackTrace();
						return false;
                    }
                    try {
                        lastName=attr.get("sn").get(0).toString();
                    } catch (NamingException e) {
                        e.printStackTrace();
						return false;
                    }
					output.append("This is from CasPluginLatest\n");

					output.append("Email = "+email + "\n");
					output.append("Surname  = "+firstName+ "\n");
					output.append("Last name = "+lastName+ "\n");
					output.append("-------------------------------------------");

                }
			} catch (NamingException e) {
				e.printStackTrace();
				return false;
			}
			return true;
	}
	
	
	
}



































 
