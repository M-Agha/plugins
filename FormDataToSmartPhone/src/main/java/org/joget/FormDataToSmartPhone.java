package org.joget;

//import org.joget.apps.app.model.AppDefinition;
//import org.joget.apps.app.model.PackageActivityForm;
import org.joget.apps.app.service.AppPluginUtil;
//import org.joget.apps.app.service.AppService;
//import org.joget.apps.app.service.AppUtil;
//import org.joget.apps.form.service.FormUtil;
import org.joget.commons.util.LogUtil;
//import org.joget.workflow.model.WorkflowAssignment;
//import org.joget.workflow.model.service.WorkflowManager;
//import org.joget.workflow.model.service.WorkflowUserManager;
//import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import java.io.File;
import java.io.IOException;
//import java.io.PrintStream;
import java.io.PrintWriter;
import java.sql.*;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.*;

//import org.joget.commons.util.TimeZoneUtil;

public class FormDataToSmartPhone extends  org.joget.plugin.base.ExtDefaultPlugin implements  org.joget.plugin.base.PluginWebSupport {

    private final static String MESSAGE_PATH = "message/FormDataToSmartPhone";
    private String form_data="";
    private String comment="";
    private Boolean thereIsAComment=false;
    //private String form_parmas="";
    public String getName() {
        return "FormDataToSmartPhone";
    }


    public String getVersion() {
        return "6.0.0";
    }


    public String getClassName() {
        return getClass().getName();
    }


    public String getLabel() {
        //support i18n
        return AppPluginUtil.getMessage("org.joget.FormDataToSmartPhone.pluginLabel", getClassName(), MESSAGE_PATH);
    }

    public String getDescription() {
        //support i18n
        return AppPluginUtil.getMessage("org.joget.FormDataToSmartPhone.pluginDesc", getClassName(), MESSAGE_PATH);
    }

    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



//        File file = new File("outFromFormDataToSmartPhonePlugin.txt");
//        PrintStream output = new PrintStream(file);
        LogUtil.info(getClassName(),"Starting LoadFromDataJsonProcess\n");

        //AppService appService = (AppService) AppUtil.getApplicationContext().getBean("appService");
       // WorkflowUserManager workflowUserManager = (WorkflowUserManager) AppUtil.getApplicationContext().getBean("workflowUserManager");

        //String activityId = request.getParameter("activityId");

       // workflowUserManager.setCurrentThreadUser(username);

        //LogUtil.info(getClassName(),"activityId from Parameters " + activityId + "\n");

        //WorkflowManager workflowManager = (WorkflowManager) AppUtil.getApplicationContext().getBean("workflowManager");

//        WorkflowAssignment wfAssignment = null;
//        int count = 0;
//        do {
//            wfAssignment = workflowManager.getAssignment(activityId);
//            //LogUtil.info(getClassName(),"wfAssignment is:" + wfAssignment + " try number " + count + "\n");
//            if (wfAssignment == null) {
//                try {
//                    Thread.sleep(4000); //wait for assignment creation
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            count++;
//        } while (wfAssignment == null && count < 5); // try max 5 times
        //LogUtil.info(getClassName(),"wfAssignment is:" + wfAssignment + "\n");

        //String processDefId = wfAssignment.getProcessDefId();
        //LogUtil.info(getClassName(),"processDefId: " + processDefId + "\n");

        //String activityDefId = wfAssignment.getActivityDefId();
        //LogUtil.info(getClassName(),"activityDefId: " + activityDefId + "\n");


        //AppDefinition appDef = appService.getAppDefinitionForWorkflowActivity(activityId);

        //String appId = appDef.getAppId();
        //LogUtil.info(getClassName(),"appId from Parameters " + appId + "\n");

        //String version = appDef.getVersion().toString();
        //LogUtil.info(getClassName(),"version from Parameters " + version + "\n");

        //PackageActivityForm activityForm = appService.retrieveMappedForm(appId, version, processDefId, activityDefId);

        //String formDefId = activityForm.getFormId();
        //LogUtil.info(getClassName(),"formDefId: " + formDefId + "\n");

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        JSONObject responseObj = new JSONObject();
        //String processId = wfAssignment.getProcessId();
        String processId = request.getParameter("processId");
        String channel=request.getParameter("channel");
        String type=request.getParameter("type");
        //String channel = request.getParameter("channel");
        //String username = request.getParameter("username");
        //LogUtil.info(getClassName(),"processId from Parameters " + processId + "\n");

        //String primaryKey = appService.getOriginProcessId(processId);
        //LogUtil.info(getClassName(),"primaryKey: " + primaryKey + "\n");

        //String form_data = "Before Retrieving data";
//        try {
//            //form_data = FormUtil.loadFormDataJson(appId, version, formDefId, primaryKey, true, true, true, wfAssignment);
//
//        } catch (JSONException e) {
//            //e.printStackTrace();
//            responseObj.put("status", "Form Data Retrieval Failure");
//            responseObj.put("code", "101");
//            responseObj.put("message", "Error in loading form data json.");
//            out.write(responseObj.toString());
//            out.flush();
//            return;
//        }

        //Collection<Map<String, String>> Col = FormUtil.getFormColumns(appDef, formDefId);
        //LogUtil.info(getClassName(),"Form Data:\n" + form_data + "\nend\n");
        //LogUtil.info(getClassName(),"Label stuff:\n" + Arrays.toString(Col.toArray()) + "\n");


        JSONParser parser = new JSONParser();
        JSONObject fData = null;
        //JSONObject fParam = null;

//        LogUtil.info(getClassName(),"\n\n" + manipulateJsonString(form_data) + "\n\n");
        if(fetchAndCheck(processId,channel,type)) {
            try {
                fData = (JSONObject) parser.parse(form_data);
                //fParam = (JSONObject) parser.parse(form_parmas);
            } catch (ParseException e) {
                //e.printStackTrace();
                responseObj.put("status", "Form Data Retrieval Failure");
                responseObj.put("code", "102");
                responseObj.put("message", "Error in JSON manipulation to get form data.");
                out.write(responseObj.toString());
                out.flush();
                return;
            }
        }
        else{
            responseObj.put("status", "Form Data Retrieval Failure");
            responseObj.put("code", "105");
            responseObj.put("message", "Incorrect parameters");
            out.write(responseObj.toString());
            out.flush();
            return;
        }
        //JSONObject fDataToMobApp = new JSONObject();

        //LogUtil.info(getClassName(),"Before:\n" + fData + "\n");
//        String tempString;
//        String key;
//        String nestedKey;
//        JSONObject temp;
//        JSONObject nestedObj;
//        int counter = 1;
//        //boolean before = true;
//        Set keys = fData.keySet();
//        Iterator iterator = keys.iterator();
//        while (iterator.hasNext()) {
//            //System.out.println("Start of for loop");
//            key = (String) iterator.next();
//
//
//            //System.out.println("TempString: " +tempString);
//            //System.out.println("Reult from getKeyFromValue" +getKeyFromValue(Col,key));
//            if (key.matches("[-+]?\\d*\\.?\\d+") && key != null){
//                nestedObj = (JSONObject)fData.get(key);
//                Iterator it=nestedObj.keySet().iterator();
//                if (it.hasNext())
//                {
//                    nestedKey = (String) it.next();
//                    temp=new JSONObject();
//                    temp.put(getKeyFromValue(Col, nestedKey), nestedObj.get(nestedKey));
//                    fDataToMobApp.put(key, temp);
//                }
//
//            }
//            else{
//                tempString = (String) fData.get(key);
//                fDataToMobApp.put(getKeyFromValue(Col, key), tempString);
//            }
//
//
//
//            // System.out.println("After Put");
//            //iterator.remove();
//            //fData.remove(key);
//        }

//        String dateCreated = (String) fDataToMobApp.get("Date Created");
//        LogUtil.info(getClassName(),"old date: " + dateCreated + "\n");
//        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
//        Date date = null;
//        try {
//            date = formatter.parse(dateCreated);
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }
//        String dateSubmitted = TimeZoneUtil.convertToTimeZone(date,null, "yyyy-MM-dd HH:mm:ss.S");
//        LogUtil.info(getClassName(),"new date: " + dateSubmitted + "\n");
//        fDataToMobApp.put("Date Created",dateSubmitted);

        //LogUtil.info(getClassName(),"After:\n" + fDataToMobApp + "\n");
        //fDataToMobApp.put("processId",processId);

//        LogUtil.info(getClassName(),fDataToMobApp.toString() + "\n");
//        JSONObject paramObj = new JSONObject();
//        paramObj.put("processId", processId);
//        paramObj.put("appId", appId);
//        paramObj.put("version", version);


        responseObj.put("status", "Form Data Retrieval Success");
        responseObj.put("code", "100");
        responseObj.put("message", "");
        //responseObj.put("params", fParam);
        responseObj.put("form_data", fData);
        responseObj.put("thereIsAComment", thereIsAComment);
        if(thereIsAComment)
            responseObj.put("comment", comment);

        LogUtil.info(getClassName(),responseObj.toString());
        out.write(responseObj.toString());
        out.flush();

        return;
    }
    private Boolean fetchAndCheck(String processId,String channel,String type){
        try {
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jwdb?characterEncoding=UTF-8", "root", "Fyp-workflow1718");
            String query = "Select form_data,comment from submitted_form_history where processId = ?";
            // set all the preparedstatement parameters
            PreparedStatement st = con.prepareStatement(query);
            st.setString(1,processId);
            ResultSet rs=st.executeQuery();
			LogUtil.info(getClassName(),"In fetchAndCheck and the params are: "+processId+" and "+channel);

            if(rs.next()){
				LogUtil.info(getClassName(),"In true");

                form_data=rs.getString(1);
                comment=rs.getString(2);
                if(rs.wasNull()){
                    //thereIsAComment=false;//already initialized to false
                    rs.close();
                    st.close();
                    con.close();
                    return true;
                }
                //form_parmas=rs.getString(2);
            }
            else{
                LogUtil.info(getClassName(),"In false");
                rs.close();
                st.close();
                con.close();
                return false;
            }
            if(type.equals("submitter")){
                thereIsAComment=true;
                return true;
            }
            // set all the preparedstatement parameters
            st.close();
            rs.close();
            query = "Select count(*) from submitted_form_history S join approvers A on S.processId=A.processId and A.approver_number=S.last_approver_number and S.processId = ? and  A.channel=?";
            st = con.prepareStatement(query);
            st.setString(1,processId);
            st.setString(2,channel);
            rs=st.executeQuery();

            if(rs.next()){
                int count=rs.getInt(1);
                if(count==1){//should be one only
                    thereIsAComment=true;
                }
            }
            else{
                LogUtil.info(getClassName(),"rs had no next in count statement");
                rs.close();
                st.close();
                con.close();
                return false;
            }
            rs.close();
            st.close();
            con.close();
        }catch (SQLException se){
            LogUtil.error(getClassName(),se,"In fetchAndCheck");
		    return false;
        }
        return true;
    }
}

