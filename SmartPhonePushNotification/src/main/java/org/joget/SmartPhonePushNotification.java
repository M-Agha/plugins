package org.joget;

import java.io.*;

import java.sql.*;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
//import javax.mail.MessagingException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import okhttp3.*;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.joget.apps.app.lib.UserNotificationAuditTrail;
//import org.joget.apps.app.model.AppDefinition;
import org.joget.apps.app.model.AppDefinition;
import org.joget.apps.app.model.AuditTrail;
//import org.joget.apps.app.model.PackageActivityForm;
import org.joget.apps.app.model.PackageActivityForm;
import org.joget.apps.app.service.AppPluginUtil;
import org.joget.apps.app.service.AppService;
import org.joget.apps.app.service.AppUtil;
//import org.joget.apps.form.model.Form;
//import org.joget.apps.form.model.FormRow;
//import org.joget.apps.form.model.FormRowSet;
import org.joget.apps.form.service.FormUtil;
import org.joget.apps.userview.model.ExtElement;
import org.joget.commons.util.LogUtil;
import org.joget.commons.util.PluginThread;
//import org.joget.commons.util.StringUtil;
import org.joget.commons.util.StringUtil;
import org.joget.commons.util.TimeZoneUtil;
import org.joget.directory.model.User;
import org.joget.directory.model.service.DirectoryManager;
import org.joget.plugin.property.model.PropertyEditable;
import org.joget.workflow.model.WorkflowActivity;
import org.joget.workflow.model.WorkflowAssignment;
import org.joget.workflow.model.WorkflowVariable;
import org.joget.workflow.model.service.WorkflowManager;
import org.joget.workflow.model.service.WorkflowUserManager;
import org.joget.apps.form.model.FormData;

import org.joget.apps.app.dao.UserReplacementDao;
import org.joget.apps.app.model.UserReplacement;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
/*
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.util.Map;
import java.util.HashMap;
import org.joget.plugin.base.Plugin;
import org.joget.plugin.base.PluginManager;
import org.joget.workflow.util.WorkflowUtil;*/


public class SmartPhonePushNotification extends UserNotificationAuditTrail {
    private final static String MESSAGE_PATH = "message/SmartPhonePushNotification";
    //private String submitterUsername="";
    ////String approverUsername="";
    //private String dateSubmitted="";
    private String processId="";
    private Connection con;
    private String channel="";
    @Override
    public String getName() {
        return "SmartPhone Push Notification";
    }

    @Override
    public String getVersion() {
        return "6.0.0";
    }

    @Override
    public String getClassName() {
        return getClass().getName();
    }

    @Override
    public String getLabel() {
        //support i18n
        return AppPluginUtil.getMessage("org.joget.SmartPhonePushNotification.pluginLabel", getClassName(), MESSAGE_PATH);
    }

    public String getDescription() {
        //support i18n
        return AppPluginUtil.getMessage("org.joget.SmartPhonePushNotification.pluginDesc", getClassName(), MESSAGE_PATH);
    }

//    @Override
//    public String getPropertyOptions() {
//        return AppUtil.readPluginResource(getClassName(), "/properties/smartPhonePushNotification.json", null, true, MESSAGE_PATH);
//    }


    @Override
    protected void sendEmail (final Map props, final AuditTrail auditTrail, final WorkflowManager workflowManager, final List<String> users, final WorkflowActivity wfActivity) {
        new PluginThread(new Runnable() {

            public void run() {

                WorkflowUserManager workflowUserManager = (WorkflowUserManager) AppUtil.getApplicationContext().getBean("workflowUserManager");
                String activityId = wfActivity.getId();

//                File file1 = new File("outFormData.txt");
//                PrintStream o = null;
//                try {
//                    o = new PrintStream(file1);
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }

               // WorkflowAssignment wfAssignment = null;
//                int count = 0;
//                do {
//                    wfAssignment = workflowManager.getAssignment(activityId);
//                    //LogUtil.info(getClassName(),"wfAssignment is:" + wfAssignment + " try number " + count + "\n");
//                    if (wfAssignment == null) {
//                        try {
//                            Thread.sleep(4000); //wait for assignment creation
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    count++;
//                } while (wfAssignment == null && count < 5); // try max 5 times
                AppService appService = (AppService) AppUtil.getApplicationContext().getBean("appService");

                AppDefinition appDef = appService.getAppDefinitionForWorkflowActivity(activityId);
                //System.out.println("wfAssignment is :" +wfAssignment);
                String appId = appDef.getAppId();
                String version=appDef.getVersion().toString();

                try {


                    //System.out.println("printing database data \n\n\n\n");
                    LogUtil.info(getClassName(),"printing database data \n\n\n\n");
                    con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jwdb?characterEncoding=UTF-8", "root", "Fyp-workflow1718");

                    for (String username : users) {
                        //approverUsername=username;
                        Collection<String> addresses = AppUtil.getEmailList(null, username, null, null);

                        if (addresses != null && addresses.size() > 0) {
                            workflowUserManager.setCurrentThreadUser(username);
                            WorkflowAssignment wfAssignment;

                            int count = 0;
                            do {
                                wfAssignment = workflowManager.getAssignment(activityId);

                                if (wfAssignment == null) {
                                    Thread.sleep(4000); //wait for assignment creation
                                }
                                count++;
                            } while (wfAssignment == null && count < 5); // try max 5 times

                            //if (wfAssignment != null) {
                            //String processId = wfAssignment.getProcessId();
                            // LogUtil.info(getClassName(),"processId: "+processId+"\n");


                            //AppService appService = (AppService) AppUtil.getApplicationContext().getBean("appService");
                            //AppDefinition appDef=appService.getAppDefinitionForWorkflowActivity(activityInstanceId);
                            //sAppUtil.getCurrentAppDefinition();
                            //String appId=appDef.getAppId();
                            //LogUtil.info(getClassName(),"appId: "+appId+"\n");
                            String processDefId = wfAssignment.getProcessDefId();

                            String activityDefId = wfAssignment.getActivityDefId();
                            PackageActivityForm activityForm = appService.retrieveMappedForm(appId, version, processDefId, activityDefId);

                            String formDefId = activityForm.getFormId();
                            processId = wfAssignment.getProcessId();
                            //LogUtil.info(getClassName(),"processId from Parameters " + processId + "\n");

                            String primaryKey = appService.getOriginProcessId(processId);


                            //get data from joget and save it into database
                            //con
                            if(!inDatabase()){
                                prepareEntry();
                                LogUtil.info(getClassName(),"Leaving the plugin since this is for submitter");
                                con.close();
                                return;
                            }




                            //LogUtil.info(getClassName(),"testing FIle\n");version,appId,

                            //String form_data="Something+went+wrong+with+fetching+form+data";
                            //**Added Here**\\
                            //String form_data = "ProcessID:" + processId + "ActivityId:+" + activityInstanceId + "appId:" + appId + "Version:" + appDef.getVersion().toString() + "ActivityDefID:" + activityDefId;
                            //String Parameters="\"processID:"+processId+",activityId:"+activityInstanceId+",appId:"+appId+",version:"+appDef.getVersion().toString()+",username:"+username+"\"";
                            String Parameters = "\"activityId:" + activityId + ",username:" + username +",processId:"+processId +",appId:"+appId+",version:"+version+"\"";
                            //String Parameters = "\"processId:" + processId + "\"";


                            //.close();


//                            File file = new File("outFromSmartPlugin.txt");
//                            PrintStream output = new PrintStream(file);
                            LogUtil.info(getClassName(),"This is from SmartPlugins\n");
                            LogUtil.info(getClassName(),username);
                            LogUtil.info(getClassName(),"\n");


//                                UserReplacementDao urDao = (UserReplacementDao) AppUtil.getApplicationContext().getBean("userReplacementDao");
//                                String args[] = wfAssignment.getProcessDefId().split("#");
//                                Collection<UserReplacement> replaces = urDao.getUserTodayReplacedBy(username, args[0], args[2]);
//                                if (replaces != null && !replaces.isEmpty()) {
//                                    for (UserReplacement ur : replaces) {
//                                        Collection<String> emails = AppUtil.getEmailList(null, ur.getReplacementUser(), null, null);
//                                        if (emails != null && !emails.isEmpty()) {
//                                            addresses.addAll(emails);
//                                        }
//                                    }
//                                }
                            String emailToOutput = "";
                            LogUtil.info(getClassName(),"After User Replacement\n");
                            for (String address : addresses) {
                                //post to acceptto app a push notification with these addresses
                                //String urlString="https://mfa.acceptto.com/api/v9/authenticate_with_options?auth_type=1&message="+form_data+"&type=Payment&email="+address+"&uid=b0bcf04bc8fe34d93cf89068fc5a1bd50b4fd26a413df7ab70f980079383c35a&secret=46cc46601e5e1fe12a2c86c8f6418e76342124bc61cadca6fc88568a5f5fd12e&timeout=31536000000";
                                String content = "{\"message\":" + Parameters + ", \"email\":\"" + address + "\",\"uid\":\"b0bcf04bc8fe34d93cf89068fc5a1bd50b4fd26a413df7ab70f980079383c35a\",\"secret\":\"46cc46601e5e1fe12a2c86c8f6418e76342124bc61cadca6fc88568a5f5fd12e\",\"type\":\"Approver\",\"auth_type\":1,\"timeout\":\"31536000000\"}";
                                LogUtil.info(getClassName(),"Content variable:\t" + content + "\n");
                                OkHttpClient client = new OkHttpClient();
                                MediaType mediaType = MediaType.parse("application/json");
                                RequestBody body = RequestBody.create(mediaType, content);
                                Request request = new Request.Builder()
                                        .url("https://mfa.acceptto.com/api/v9/authenticate_with_options").post(body)
                                        .addHeader("content-type", "application/json")
                                        .addHeader("cache-control", "no-cache")
                                        .build();


                                // CloseableHttpClient httpclient = HttpClients.createDefault();
                                //HttpGet httppost = new HttpGet(urlString);
                                LogUtil.info(getClassName(),"Before Execute\n");
                                // httpclient.execute(httppost);
                                int trys = 0;
                                while (trys < 5) {
                                    try (Response response = client.newCall(request).execute()) {
                                        if (response.isSuccessful()) {
                                            String resString = response.body().string();
                                            LogUtil.info(getClassName(),resString + "\n");
                                            JSONParser parser = new JSONParser();
                                            JSONObject res = (JSONObject) parser.parse(resString);
                                            channel=res.get("channel").toString();
                                            //LogUtil.info(getClassName(),"THis is the channel: "+channel + "\n");
                                            break;
                                        } else {
                                            trys++;
                                            LogUtil.info(getClassName(),"Acceptto Problem: Response is not successful for try number:" + trys + "\n");
                                            Thread.sleep(4000);
                                        }
                                    }
                                }
                                if (trys > 4) {
                                    LogUtil.info(getClassName(),"No more Trys; Acceptto Server probably down\n");
                                }
//                                    HttpURLConnection urlConnection = null;
//                                    URL url = new URL(urlString);
//                                    urlConnection = (HttpURLConnection) url.openConnection();
//                                    urlConnection.setRequestMethod("GET");
//                                    urlConnection.setReadTimeout(10000 / 1000 );
//                                    urlConnection.setConnectTimeout(15000 / 1000);
//                                    urlConnection.setDoOutput(true);
//                                    urlConnection.connect();

                                emailToOutput += address + ", ";
                            }
                            LogUtil.info(getClassName(),"Email to send to: " + emailToOutput + "\n");

                            if(isEntryNullInDataBase()){
                                LogUtil.info(getClassName(),"Entry is null in database");

                                String form_data = FormUtil.loadFormDataJson(appId, version, formDefId, primaryKey, true, true, true, wfAssignment);

                                Collection<Map<String, String>> Col = FormUtil.getFormColumns(appDef, formDefId);

                                fetchAndStoreToDatabase(form_data,Col,username);
                                //Also push a notification to submitter here
                            }
                            else{
                                insertInfoToDatabase(username);
                            }
                            //output.close();
                            //if(!addChannelToDataBase(channel)){
                             //   LogUtil.info(getClassName(), "Could not add channel to database");
                            //}
                            //} else {
                            //   LogUtil.debug(UserNotificationAuditTrail.class.getName(), "Fail to retrieve assignment for " + username);
                            // }
                        }
                    }
                    con.close();
                }catch(JSONException e){
                    LogUtil.error(UserNotificationAuditTrail.class.getName(), e, "Error from json form data funciton");

                } catch (Exception e) {
                    LogUtil.error(UserNotificationAuditTrail.class.getName(), e, "Error executing plugin");
                }
            }
        }).start();
    }
	/* Not needed anymore
    private Boolean addChannelToDataBase(String channel){
        if(channel.equals("")){
            return false;
        }
        try {
            String query = "insert into channel (channel,processId) VALUES (?,?)";
            PreparedStatement st = con.prepareStatement(query);
            st.setString(1,channel);
            st.setString(2,processId);
            st.executeUpdate();
            st.close();
            return true;
        }catch (SQLException se){
            LogUtil.error(getClassName(),se,"In addChannelToDataBase");
        }

        return false;
    }
	*/
    private void prepareEntry(){//this functions only add the processId to tell approvers that it has passed to the approver stage
        try {
            String query = "insert into submitted_form_history(processId) VALUES(?)";
            PreparedStatement st = con.prepareStatement(query);
            st.setString(1,processId);
            st.executeUpdate();
            st.close();
        }catch (SQLException se){
            LogUtil.error(getClassName(),se,"In prepareEntry");
        }
    }
    private Boolean isEntryNullInDataBase(){
        try {
            LogUtil.info(getClassName(),"Inside function isEntryNullInDataBase");

            String query = "Select form_data from submitted_form_history where processId = ?";
            PreparedStatement st = con.prepareStatement(query);
            st.setString(1,processId);
            ResultSet rs=st.executeQuery();
            rs.next();
            LogUtil.info(getClassName(),"Result "+rs.getString(1));
            LogUtil.info(getClassName(),"Entry was null? "+rs.wasNull());
            return rs.wasNull();
        }catch (SQLException se){
            LogUtil.error(getClassName(),se,"In isEntryNullInDataBase");
        }
        return true;
    }
    private Boolean inDatabase(){//this function checks if there is an entry in the database to know if this is the event for the submitter or approver

        try {
            String query = "Select count(*) from submitted_form_history where processId = ?";
            // set all the preparedstatement parameters
            PreparedStatement st = con.prepareStatement(query);
            st.setString(1,processId);

            // execute the preparedstatement insert
            ResultSet rs=st.executeQuery();
            rs.next();
            int rowCount = rs.getInt(1);
            st.close();
            LogUtil.info(getClassName(),"This is the count of rows:"+rowCount);
            return rowCount!=0;
            //con.close();
        }
        catch (SQLException se)
        {
            // log exception
            LogUtil.error(getClassName(),se,"In inDatabase");
        }
        return false;
    }

    private void fetchAndStoreToDatabase(String form_data, Collection<Map<String, String>> Col,String username){
        //GetForm data here then check
        if(form_data.length()<=2)
            return;


        //Collection<Map<String, String>> Col = FormUtil.getFormColumns(appDef, formDefId);
        LogUtil.info(getClassName(),"Form Data:\n" + form_data + "\nend\n");
        //LogUtil.info(getClassName(),"Label stuff:\n" + Arrays.toString(Col.toArray()) + "\n");


        JSONParser parser = new JSONParser();
        JSONObject fData = null;
        LogUtil.info(getClassName(),"\n\n" + manipulateJsonString(form_data) + "\n\n");
        try {
            fData = (JSONObject) parser.parse(manipulateJsonString(form_data));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        JSONObject fDataToMobApp = new JSONObject();

        //LogUtil.info(getClassName(),"Before:\n" + fData + "\n");
        String tempString;
        String key;
        String nestedKey;
        JSONObject temp;
        JSONObject nestedObj;
        //int counter = 1;
        //boolean before = true;
        Set keys = fData.keySet();
        Iterator iterator = keys.iterator();
        while (iterator.hasNext()) {
            //LogUtil.info(getClassName(),"Start of for loop");
            key = (String) iterator.next();


            //LogUtil.info(getClassName(),"TempString: " +tempString);
            //LogUtil.info(getClassName(),"Reult from getKeyFromValue" +getKeyFromValue(Col,key));
            if (key.matches("[-+]?\\d*\\.?\\d+") && key != null){
                nestedObj = (JSONObject)fData.get(key);
                Iterator it=nestedObj.keySet().iterator();
                if (it.hasNext())
                {
                    nestedKey = (String) it.next();
                    temp=new JSONObject();
                    temp.put(getKeyFromValue(Col, nestedKey), nestedObj.get(nestedKey));
                    fDataToMobApp.put(key, temp);
                }

            }
            else{
                tempString = (String) fData.get(key);
                fDataToMobApp.put(getKeyFromValue(Col, key), tempString);
            }



            // LogUtil.info(getClassName(),"After Put");
            //iterator.remove();
            //fData.remove(key);
        }


            String dateCreated = (String) fDataToMobApp.get("Date Created");
            LogUtil.info(getClassName(),"old date: " + dateCreated + "\n");
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
            Date date = null;
            try {
                date = formatter.parse(dateCreated);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            String dateSubmitted = TimeZoneUtil.convertToTimeZone(date, null, "d MMM yyyy hh:mm:ss a");
            LogUtil.info(getClassName(),"new date: " + dateSubmitted + "\n");
            fDataToMobApp.put("Date Created", dateSubmitted);



//        JSONObject fparam = new JSONObject();
//        //fparam.put("processId", processId);
//        fparam.put("appId", appId);
//        fparam.put("version", version);


//two params to add to the database
        LogUtil.info(getClassName(),"form data: "+fDataToMobApp.toString()+"\n");
        insertIntoDatabase(fDataToMobApp.toString(),username);

//        try {
//
//            LogUtil.info(getClassName(),"printing database data1 \n\n\n\n");
//
//            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jwdb?characterEncoding=UTF-8", "root", "Fyp-workflow1718");
//            Statement stmt=con.createStatement();
//            ResultSet rs=stmt.executeQuery("select * from submitted_form_history");
//            LogUtil.info(getClassName(),"printing database data2 \n\n\n\n");
//
//            while(rs.next()) {
//                //LogUtil.debug(getClassName(), rs.getString(1) + "  " + rs.getString(2) + "  " + rs.getString(3) + "  " + rs.getString(4));
//                LogUtil.info(getClassName(),"printing database data \n\n\n\n");
//                LogUtil.info(getClassName(),rs.getString(1) + "  " + rs.getString(2) + "  " + rs.getString(3) + "  " + rs.getString(4));
//            }
//
//            con.close();
//        }
//        catch(Exception e){
//            LogUtil.debug(getClassName(),e.toString());
//        }
    }

    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		File file = new File("outFromSmartPhonePushNotification.txt");
        PrintStream output = new PrintStream(file);
        LogUtil.info(getClassName(),"Starting Complete form Process\n");
		
        String activityId = request.getParameter("activityId");
		LogUtil.info(getClassName(),"activityId from Parameters " + activityId + "\n");
		
        String processId = request.getParameter("processId");
		LogUtil.info(getClassName(),"processId from Parameters " + processId + "\n");
		
		String status="";
		//LogUtil.info(getClassName(),"status from Parameters " + status + "\n");
		
        String appId = request.getParameter("appId");
		LogUtil.info(getClassName(),"appId from Parameters " + appId + "\n");
		
        String version = request.getParameter("version");
		LogUtil.info(getClassName(),"version from Parameters " + version + "\n");
		
        String username = request.getParameter("username");
		LogUtil.info(getClassName(),"username from Parameters " + username + "\n");

		String channel = request.getParameter("channel");

		String approversEmail ="";
		LogUtil.info(getClassName(),"channel from Parameters " + channel + "\n");
        LogUtil.info("SmartPhonePushNotification","channel from Parameters " + channel);

        Collection<String> addresses = AppUtil.getEmailList(null, username, null, null);

        if (addresses != null && addresses.size() > 0) {//need to fix here to handle more than one email if there is!!


            for (String address : addresses) {//for now assuming only one email
                approversEmail = address;
                //Using check method from rest api Acceptto to get the decision of form approval from the smartphone
                String content = "{\"channel\":\"" + channel + "\", \"email\":\"" + address + "\"}";
                LogUtil.info(getClassName(),"Content variable:\t" + content + "\n");


                OkHttpClient client = new OkHttpClient();
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, content);
                Request requestTOAcceptto = new Request.Builder()
                        .url("https://mfa.acceptto.com/api/v9/check").post(body)
                        .addHeader("content-type", "application/json")
                        .addHeader("cache-control", "no-cache")
                        .build();

                int trys =0;
                while(trys<5) {
                    try (Response responseFromA = client.newCall(requestTOAcceptto).execute()) {
                        if (responseFromA.isSuccessful()) {
                            String resString=responseFromA.body().string();
                            LogUtil.info(getClassName(),resString+"\n");
                            JSONParser parser = new JSONParser();

                            JSONObject res=(JSONObject)parser.parse(resString);
                            status=res.get("status").toString();
                            LogUtil.info(getClassName(),status+"\n");
                            break;
                        } else {
                            trys++;
                            LogUtil.info(getClassName(),"Acceptto Problem: Response is not successful for try number:" + trys + "\n");
                            Thread.sleep(4000);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if(trys>4){
                    LogUtil.info(getClassName(),"No more Trys; Acceptto Server probably down\n");
                }
            }
        }



        WorkflowUserManager workflowUserManager = (WorkflowUserManager) AppUtil.getApplicationContext().getBean("workflowUserManager");
        workflowUserManager.setCurrentThreadUser(username);
        LogUtil.info(getClassName(),"Current Thread User set successfully\n");

        AppService newAppService = (AppService) AppUtil.getApplicationContext().getBean("appService");
        LogUtil.info(getClassName(),"AppService Created Successfully\n");
        //Connection conn;
        int sts=0;//assume it is still pending if an error happens
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jwdb?characterEncoding=UTF-8", "root", "Fyp-workflow1718");
        }
        catch(SQLException se){
            LogUtil.error(UserNotificationAuditTrail.class.getName(), se, "Error from connection string");
        }
        Map<String, String> workflowVariableMap = new HashMap<String,String>();

        String submitterUsername = request.getParameter("submitter");
        LogUtil.info(getClassName(),"submitter from Parameters " + submitterUsername + "\n");

        /* Get the submitter's email */
        DirectoryManager dm = (DirectoryManager) AppUtil.getApplicationContext().getBean("directoryManager");
        User theSubmitter = dm.getUserByUsername(submitterUsername);
        String submitterEmail = theSubmitter.getEmail();


		if(status.equals("rejected"))
        {
            workflowVariableMap.put("status","Rejected");
            sts=2;
            String rejectionComment = request.getParameter("rejectionComment");
            LogUtil.info(getClassName(),"rejectionComment from Parameters " + rejectionComment + "\n");



            if (!rejectionComment.equals("**NONE**")){

            /* ************************* */
                ClassLoader originalClassLoader = Thread.currentThread().getContextClassLoader();
                Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());

                try {
                    String query = "UPDATE submitted_form_history set comment=? where processId=?";
                    PreparedStatement st = con.prepareStatement(query);
                    st.setString(1, rejectionComment);
                    st.setString(2, processId);
                    st.executeUpdate();
                    st.close();
                }
                catch(SQLException se){
                    LogUtil.error(UserNotificationAuditTrail.class.getName(), se, "Error from prepared statement to add comment");
                }
                try {

                    final String smtpHost = "smtp.gmail.com";
                    String smtpPort = "465";
                    String smtpUsername = "workflofyp@gmail.com";
                    String smtpPassword = "workflow1718";
                    //String security = "SSL";

                    String from = "workflofyp@gmail.com";

                    String subject = "Form Rejection Comment";
                    String emailMessage = "This is the comment: \n" + rejectionComment;

                    //create the email message
                    HtmlEmail email = new HtmlEmail();
                    email.setHostName(smtpHost);
                    email.setSmtpPort(Integer.parseInt(smtpPort));
                    email.setAuthentication(smtpUsername, smtpPassword);
                    email.setSSLOnConnect(true);
                    email.setSSLCheckServerIdentity(true);
                    email.setSslSmtpPort(smtpPort);
                    email.setSubject(subject);
                    try {
                        email.setFrom(StringUtil.encodeEmail(from));
                        email.addTo(submitterEmail);
                        email.setMsg(emailMessage);
                        email.setCharset("UTF-8");
                        email.send();
                    } catch (EmailException e) {
                        e.printStackTrace();
                    }

                } finally {
                    Thread.currentThread().setContextClassLoader(originalClassLoader);
                }
            }
        }
        else if(status.equals("approved")) {
            workflowVariableMap.put("status", "Approved");
            sts=1;
		}
        else if(status==null||status.equals("Transaction not found!"))//going to assume this never happens
            LogUtil.info(getClassName(),"Status is equal to "+status);


        if(!status.equals("Transaction not found!")&&status!=null) {

            User theApprover = dm.getUserByUsername(username);


            //********************//
            String Parameters = "\"processId:"+ processId + "\"";
            String content = "{\"message\":" + Parameters + ", \"email\":\"" + submitterEmail + "\",\"uid\":\"b0bcf04bc8fe34d93cf89068fc5a1bd50b4fd26a413df7ab70f980079383c35a\",\"secret\":\"46cc46601e5e1fe12a2c86c8f6418e76342124bc61cadca6fc88568a5f5fd12e\",\"type\":\"Submitter-" + theApprover.getFirstName() + " " + theApprover.getLastName() + "-" + status + "\",\"auth_type\":1,\"timeout\":\"31536000000\"}";
            LogUtil.info(getClassName(),"Content variable:\t" + content + "\n");
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, content);
            Request request2 = new Request.Builder()
                    .url("https://mfa.acceptto.com/api/v9/authenticate_with_options").post(body)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();


            // CloseableHttpClient httpclient = HttpClients.createDefault();
            //HttpGet httppost = new HttpGet(urlString);
            LogUtil.info(getClassName(),"Before Execute\n");
            // httpclient.execute(httppost);
            int trys = 0;
            while (trys < 5) {
                try (Response response2 = client.newCall(request2).execute()) {
                    if (response2.isSuccessful()) {
                        String resString = response2.body().string();
                        LogUtil.info(getClassName(),resString + "\n");
                        JSONParser parser = new JSONParser();
                        JSONObject res = null;
                        try {
                            res = (JSONObject) parser.parse(resString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        channel=res.get("channel").toString();
                        //LogUtil.info(getClassName(),"THis is the channel: "+channel + "\n");
                        break;
                    } else {
                        trys++;
                        LogUtil.info(getClassName(),"Acceptto Problem: Response is not successful for try number:" + trys + "\n");
                        try {
                            Thread.sleep(4000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (trys > 4) {
                LogUtil.info(getClassName(),"No more Trys; Acceptto Server probably down\n");
            }

            //********************//

            try {
                String query1 = "UPDATE approvers set decision=? where processId=? and username=? and decision =?";
                PreparedStatement st1 = con.prepareStatement(query1);
                st1.setInt(1, sts);
                st1.setString(2, processId);
                st1.setString(3, username);
                st1.setInt(4, 0);
                st1.executeUpdate();
                st1.close();
            }
            catch(SQLException se){
                LogUtil.error(UserNotificationAuditTrail.class.getName(), se, "Error from prepared statement to add decision");
            }


            FormData formResult = newAppService.completeAssignmentForm(appId, version, activityId, null, workflowVariableMap);
            LogUtil.info(getClassName(),"\nError: " + formResult.getFormErrors() + "\n");//might need to handle errors here later on
            LogUtil.info(getClassName(),"Form Completed Successfully\n");

        }
        else
            LogUtil.info(getClassName(),"Form Completion failed");
    }
    private static Object getKeyFromValue(Collection<Map<String, String>> Col, Object value) {
        for (Map<String, String> map : Col) {
//            LogUtil.info(getClassName(),map.get("value"));
//            LogUtil.info(getClassName(),value);

            if (map.get("value").equals(value)) {
                //LogUtil.info(getClassName(),map.get("label"));
                return map.get("label");
            }
        }
        return null;
    }
    private static String manipulateJsonString(String formData) {
        int index=1;
        int numb=1;
        String newFormData="{";
        while(index<formData.length()){
            if(checkStringAtIndex(formData,index+1,"createdBy")){
                for(int j=index;j<formData.length();j++)
                    newFormData+=formData.charAt(j);
                return newFormData;
            }
            if(formData.charAt(index)=='\"'){
                newFormData+="\""+numb+"\":{";
                numb++;
                /*int tempIndex = index;
                while (formData.charAt(tempIndex)!=','&&formData.charAt(index)!='}'){

                    tempIndex++;
                }*/
                //while(formData.charAt(index)!=','&&formData.charAt(index)!='}'){
                while(formData.charAt(index)!='}'){
                    if (formData.charAt(index)==',' && formData.charAt(index-1)=='\"' && formData.charAt(index+1)=='\n' && formData.charAt(index+6)=='\"' )
                    {
                        break;
                    }
                    else {
                        newFormData+=formData.charAt(index);
                        index++;
                    }
                }
                newFormData+="}";
            }
            newFormData+=formData.charAt(index);
            index++;
        }
        return newFormData;
    }
    private static boolean checkStringAtIndex(String str,int index,String check) {
        for(int i=0;i<check.length();i++){
            if(str.charAt(index+i)!=check.charAt(i))
                return false;
        }
        return true;
    }
    private void insertIntoDatabase (String fData,String username){


        String query = "UPDATE submitted_form_history set form_data=?, last_approver_number=last_approver_number +1 where processId=?";
        String query1 = "Insert into approvers (channel,processId,username,approver_number)  select ?,?,?,last_approver_number from submitted_form_history where processId= ? ";

        try {

            // set all the preparedstatement parameters
            PreparedStatement st = con.prepareStatement(query);
            st.setString(1,fData);
            //st.setString(2,fParams);
            st.setString(2,processId);


            // execute the preparedstatement insert
            st.executeUpdate();
            st.close();
            st=con.prepareStatement(query1);
            st.setString(1,channel);
            st.setString(2,processId);
            st.setString(3,username);
            st.setString(4,processId);
            st.executeUpdate();
            st.close();
        }
        catch (SQLException se)
        {
            // log exception
            LogUtil.error(getClassName(),se,"In insertIntoDatabase");
        }

    }
    private void insertInfoToDatabase (String username){


        String query = "UPDATE submitted_form_history set last_approver_number=last_approver_number +1 where processId=?";
        String query1 = "Insert into approvers (channel,processId,username,approver_number)  select ?,?,?,last_approver_number from submitted_form_history where processId= ? ";

        try {

            // set all the preparedstatement parameters
            PreparedStatement st = con.prepareStatement(query);
            //st.setString(2,fParams);
            st.setString(1,processId);


            // execute the preparedstatement insert
            st.executeUpdate();
            st.close();
            st=con.prepareStatement(query1);
            st.setString(1,channel);
            st.setString(2,processId);
            st.setString(3,username);
            st.setString(4,processId);
            st.executeUpdate();
            st.close();
        }
        catch (SQLException se)
        {
            // log exception
            LogUtil.error(getClassName(),se,"In insertInfotoDatabase");
        }

    }
}
