package org.joget.MobileAppAuthenticationPlugin;

import org.joget.apps.app.service.AppPluginUtil;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Properties;

public class MobileAppAuthenticationPlugin  extends  org.joget.plugin.base.ExtDefaultPlugin implements  org.joget.plugin.base.PluginWebSupport{
    private final static String MESSAGE_PATH = "message/MobileAppAuthenticationPlugin";

    public String getName() {
        return "MobileAppAuthenticationPlugin";
    }


    public String getVersion() {
        return "6.0.0";
    }


    public String getClassName() {
        return getClass().getName();
    }


    public String getLabel() {
        //support i18n
        return AppPluginUtil.getMessage("org.joget.MobileAppAuthenticationPlugin.pluginLabel", getClassName(), MESSAGE_PATH);
    }

    public String getDescription() {
        //support i18n
        return AppPluginUtil.getMessage("org.joget.MobileAppAuthenticationPlugin.pluginDesc", getClassName(), MESSAGE_PATH);
    }

    public void webService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        File file = new File("outFromMobileAppAuthenticationPlugin.txt");
        PrintStream output = new PrintStream(file);
        output.append("hellloooo");
        /*String username = request.getParameter("username");
        String password = request.getParameter("password");

        String realUsername = null;
        int i=0;
        while (username.charAt(i) != '@'){
            realUsername = realUsername + username.charAt(i);
        }
        output.append(realUsername);

        Properties initialProperties = new Properties();
        initialProperties.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
        initialProperties.put(Context.PROVIDER_URL, "ldap://localhost:389");
        initialProperties.put(Context.SECURITY_AUTHENTICATION, "simple");
        initialProperties.put(Context.SECURITY_PRINCIPAL, "CN=M.Agha,CN=Users,dc=fypworkflow,dc=westeurope,dc=cloudapp,dc=azure,dc=com");
        initialProperties.put(Context.SECURITY_CREDENTIALS, "Fyp-workflow1718");
        DirContext context = null;

        output.append("Username Check - Before Context try catch\n");
        try {
            context = new InitialDirContext(initialProperties);
        } catch (NamingException e) {
        }
        output.append("Username Check - After Context try catch\n");

        String searchFilter="(CN="+username+")";
        String[] requiredAttributes={"mail"};
        SearchControls controls=new SearchControls();
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        controls.setReturningAttributes(requiredAttributes);
        NamingEnumeration users=null;

        output.append("Username Check - Before Search\n");

        try {
            users = context.search("CN=Users,dc=fypworkflow,dc=westeurope,dc=cloudapp,dc=azure,dc=com", searchFilter, controls);
        } catch (NamingException e) {
            e.printStackTrace();
        }

        output.append("Username Check - After Search\n");

        SearchResult searchResult=null;
        String email=null;
        if(users==null) {
            output.append("Username Check - User Not Found\n");
            PrintWriter out = response.getWriter();
            out.write("Failure");
            out.flush();
            return;
        }

        output.append("Credentials Authentication - Start\n");

        try {
            Properties newInitialProperties = new Properties();
            newInitialProperties.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
            newInitialProperties.put(Context.PROVIDER_URL, "ldap://localhost:389");
            newInitialProperties.put(Context.SECURITY_AUTHENTICATION, "simple");
            newInitialProperties.put(Context.SECURITY_PRINCIPAL, "CN=" + username + ",CN=Users,dc=fypworkflow,dc=westeurope,dc=cloudapp,dc=azure,dc=com");
            newInitialProperties.put(Context.SECURITY_CREDENTIALS, password);
            DirContext newContext = null;
            newContext = new InitialDirContext(initialProperties);

        } catch (NamingException e) {
            output.append("Credentials Authentication - Failed\n");
            PrintWriter out = response.getWriter();
            out.write("Failure");
            out.flush();
            e.printStackTrace();
            return;
        }
        PrintWriter out = response.getWriter();
        out.write("Success");
        out.flush();
        output.append("Credentials Authentication - Success\n");
        */
    }
}